Montaño Bautista Marco Alan
308315569

Reporte

1.Se realiza la instalación del SO Debian en una maquina virtual usando VirtualBox, se usan las configuraciones recomendadas en los lineamientos.
![alt text](https://gitlab.com/Alan050/ReporteKernel/blob/bf084e489c9e1ee97cf1b9bdb4858b088c38d8c9/img/CapturaInstalacion.png "uno")

2.Verificamos los archivos en etc/apt.

3.Actualizamos el sistema.
![alt text](https://gitlab.com/Alan050/ReporteKernel/blob/dbd59326ec2df22c7479bae08f3390c83764d165/img/Actualizando.png "dos")

4.Instalamos los paquetes build-essential fakeroot y libn.

5.Descargamos el Kernel de linux.
![alt text](https://gitlab.com/Alan050/ReporteKernel/blob/dbd59326ec2df22c7479bae08f3390c83764d165/img/DescargaKernel.png "tres")

6.Descargamos la firma GPG.

7.Verificaremos el kernel, primero descomprimimos el archivo xz, luego cotejamos el archivo .sign y nos arroja la salida esperada,
Tenemos que descargar la llave PGP del servidor usando nuestra Key id, volvemos a correr el comando verify y nos arroja nuestra llave.
![alt text](https://gitlab.com/Alan050/ReporteKernel/blob/dbd59326ec2df22c7479bae08f3390c83764d165/img/Verificacion2Completa.png "cuatro")

8.Desempaquetamos el tarball del kernel.

9.Copiamos la configuracion del kernel actual para tomarla como base para el nuevo kernel que vamos a compilar.
![alt text](https://gitlab.com/Alan050/ReporteKernel/blob/dbd59326ec2df22c7479bae08f3390c83764d165/img/CopiarKernel.png "cinco")

10.Accedemos al menu de configuracion del kernel.
![alt text](https://gitlab.com/Alan050/ReporteKernel/blob/dbd59326ec2df22c7479bae08f3390c83764d165/img/MenuConfigKernel.png "seis")

11.Realizamos cambios al nombre de la version local y al default hostname.
![alt text](https://gitlab.com/Alan050/ReporteKernel/blob/dbd59326ec2df22c7479bae08f3390c83764d165/img/MenuConfigKernelLocalVersion.png "siete")

12.Realizamos el make help.

13.Usamos make all para compilar el kernel.
![alt text](https://gitlab.com/Alan050/ReporteKernel/blob/dbd59326ec2df22c7479bae08f3390c83764d165/img/CompilandoElkernel.png "ocho")

14.Verificamos con echo $? que make no haya tenido errores.

15.Ejecutamos make deb-pkg para generar los paquetes necesarios para instalar nuestro kernel.
![alt text](https://gitlab.com/Alan050/ReporteKernel/blob/dbd59326ec2df22c7479bae08f3390c83764d165/img/MakeDeb.png "nueve")

16.Notese el tamaño de los archivos en particular linux-image-3.16.50marco-montano-ciencias-unam que tiene tamaño de 315Mbs.

17.Podemos acceder a la informacion de cada paquete usando --info "paquete".

18.Instalamos los paquetes con dpkg.

19.Listamos los paquetes intalados.
![alt text](https://gitlab.com/Alan050/ReporteKernel/blob/dbd59326ec2df22c7479bae08f3390c83764d165/img/PauqetesInstalados.png "diez")

20.Verificamos el contenido de /boot.

21.Verificamos la configuracion del cargador de inicio grub.

22.Reiniciamos la maquina para verificar el neuvo kernel.

23.Una vez inciciado el SO verificamos la version de kernel que estamos usando. 
![alt text](https://gitlab.com/Alan050/ReporteKernel/blob/dbd59326ec2df22c7479bae08f3390c83764d165/img/NuevoKernel.png "once")

DIFICULTADES: Me fue imposible encontar algunos de los archivos que van en la carpeta build y otros no se si sean los correctos, tuve problemas al tartar de usar aptitude en un princpio por un bug de la maquina virtual que te solicita tener el "disco" de instalacion del sistema, esto lo resolvi montando el iso en la unidad de cd
